import numpy as np
import random
from time import sleep

def colors(num_lists,list_length):

    a = ['#FF0000','#008000', '#0000CD', '#D2691E', '#808000']
    b = ['#800080', '#FF4500', '#000000', '#DAA520', '#7CFC00']
    c = ['#00BFFF', '#FF8C00', '#DC143C', '#90EE90', '#8B0000']
    d = ['#20B2AA', '#2F4F4F', '#8B4513', '#FF69B4', '#A9A9A9']

    color_lists = [a, b, c, d]

    while num_lists > len(color_lists):
        color_lists.append([])

    for clist in color_lists:
        while list_length > len(clist):
            clist.append((random.random(),random.random(),random.random()))
            sleep(0.65168315)



    return color_lists

