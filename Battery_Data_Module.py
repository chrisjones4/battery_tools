import plot_colors 
try:
    import matplotlib
    matplotlib.use('TkAgg')
    import glob
    import matplotlib.pyplot as plt
    import numpy as np
    import os
    import pandas as pd
    from pandas import ExcelFile
    from pandas import ExcelWriter
    import sys
    import random as rand
    import scipy
    from scipy.signal import savgol_filter
    import time
    from time import sleep
    
except Exception as e:
    print('')
    print(e)
    print('')
    print('*****')
    print('It appears that you do not have one or more of the required Python modules installed.')
    print('As a result, you may encounter errors when trying to run this code.')
    print('*****')
    print('')
    print('-Check the README file for instructions and requirements to use this program.')
    print('-If you are still having issues, email chrisjones4@u.boisestate.edu with any questions.')
    print('------------------')
    print('')


class File:

    num_files = 0

    def __init__(self, fileName):

        print('')
        print('Loading {} -- This may take a while'.format(fileName))
        print('')
        self.excelFile = pd.ExcelFile(fileName)  # Loads the excel file into memory
        
        # General info/variables unique to each file #
        
        self.lastSheet = pd.read_excel(self.excelFile, sheet_name=-1) # Last worksheet. Summary of all cycles completed
        self.information = pd.read_excel(self.excelFile, sheet_name=0, skiprows=3) # First worksheet. Contains some initial battery information
        self.sheet_num = len(self.excelFile.sheet_names) - 1     # Number of worksheets in the Excel file minus 1
        self.comments = np.array(self.information['Comments'])  # User input comments before cycling the battery
        self.cycle_list = np.array(self.lastSheet['Cycle_Index'])
        self.num_cycles = self.cycle_list[-1]   # Number of completed cycles for this battery
        self.first_worksheet = pd.read_excel(self.excelFile, sheet_name=1)   # Loads the first worksheet *containing cycle information*
                                                                                # Is actually the 2nd worksheet of the excel file
        self.current = np.array(self.first_worksheet['Current(A)'])
        self.first_current = np.argmax(self.current != 0)   # Finds the first index where the current != 0
        self.initial_current = self.current[self.first_current]     # Returns the value of the current at the above index
                                                                    # Only interested in the sign of the value (see below)
        if self.initial_current > 0:  # Determines the battery type (Half cell or Full cell)
            self.battery_type = 'full cell' 
        elif self.initial_current < 0:
            self.battery_type = 'half cell'
        
        print('BATTERY INFORMATION:') # Prints out some useful information about the battery, and asks for file label
        print('--------------------')
        print('Cycles Completed: {} '.format(self.num_cycles))
        print('')
        print('Comments:')
        print(self.comments)
        print('--------------------')
        print('')
        print('Enter a label for ' + str(fileName)) 
        self.label = input()   # Create a label for the file

        a = 0
        while a == 0:
            print('Enter the active weight for {} ({}) in grams'.format(fileName,self.label)) 
            try:
                self.activeWeight = float(input())   # Active Weight
                a = 1
            except Exception:
                print('')
                print('The active weight input was not valid.')
                print('Only enter numbers; do not include any unit symbols or letters.')
                print('')
                a = 0
        
        File.num_files += 1

def check_files(fname):
    
    if len(glob.glob('{}*.x*'.format(fname))) >= 2: # .xlsx and .xls files exist with same filename
        excelFileName = 1
    elif glob.glob('{}*'.format(fname)) == []: # The file name entered does not exist in any type of format
        excelFileName = 0
    elif glob.glob('{}*'.format(fname))[0].split('.')[1] != "xlsx" and glob.glob('{}*'.format(fname))[0].split('.')[1] != 'xls':
        excelFileName = 0 # The file name entered exists, but not as an excel file
    else:
        excelFileName =  glob.glob('{}*'.format(fname))[0] # file lookup was successful

    return excelFileName
             
def get_started():  # User enters the files to be opened. Create class instances
    
    excelFiles = []  # List of only the files that exist in .xls or .xlsx - Determined by the for loop below

    while len(excelFiles) == 0:
        print('Type the name of the file(s) you want to import. If more than one, '
              'separate with a comma. (Caps Sensitive)')
        file_names = [x for x in input().split(',')]  # List of user input file names
        num_files = len(file_names)
        for n in file_names:
            excelFileName = check_files(n)
            if excelFileName != 0 and excelFileName != 1:
                excelFiles.append(excelFileName)
            elif excelFileName == 0:
                print('')
                print(('{} could not be found, or is not saved in the required .xlsx or .xls format').format(n))
                print('')
                print('Double check the file name and/or file type and try again.')
                print('')
                print('REMEMBER: The files you want to load have to be saved in the same folder as the Python program.')
                print('')
                excelFiles.clear()
            elif excelFileName == 1:
                print('')
                print('There appears to be multiple files with the same file name, '
                      'but different file types (.xlsx and .xls)')
                print('')
                print('You can specify the file you want to open by including the extension (.xls or .xlsx).')
                print('')
                excelFiles.clear()
                
        if num_files == len(excelFiles):
            break
        else:
            excelFiles.clear()

    files = [File(n) for n in excelFiles]  # Create instance for each file

    if os.path.exists('Outputs'):
        pass
    else:  # Create an Outputs folder if it doesn't exist
        os.mkdir('Outputs')

    return files

def get_column_info(charge_type, column_name, index, i):  # Pulls from all worksheets between first & last
                    
    currIndex = 0
    retVal = []
    for sheet in range(1, files[i].sheet_num):
        if currIndex > index:
            break
        ws = pd.read_excel(files[i].excelFile, sheet_name=sheet)
        value = np.array(ws[column_name])
        index_list = np.array(ws['Cycle_Index'])
        current = np.array(ws['Current(A)'])
        n = 0

        while currIndex <= index and n < len(index_list):
            if charge_type == 'charge':
                if(index_list[n] == index) and (current[n] > 0):
                    retVal.append(value[n])
                currIndex = index_list[n]
                n = n + 1   
            elif charge_type == 'discharge':
                if(index_list[n] == index) and (current[n] < 0):
                    retVal.append(value[n])
                currIndex = index_list[n]
                n = n + 1
                
    retVal = np.array(retVal)
    return retVal


def individual_cycles():
    
    print('')
    a = 0
    while a == 0:  # The cycle numbers that will be used in for loops below during plotting
        print('Type the cycle numbers you want to plot or compare (separate cycles with a comma)')
        try:
            compare_cycles = [int(x) for x in input().split(",")]
            for i in range(0, (len(files))):
                for n in compare_cycles:  # Handles input of cycle that hasn't completed
                    if n > files[i].num_cycles or n <= 0:
                        print('Cycle {} has not been completed for battery {}'.format(n, files[i].label))
                        print('')
                        print('Re-enter the cycle numbers.')
                        print('')
                        compare_cycles.clear()
                        a = 0
                        break
                    else:
                        a = 1
        except ValueError:  # Handles invalid non-integer inputs
            print('That was not a valid input for cycle number. Try again.')
            a = 0
            
    return compare_cycles

def voltage_profile(charge_type, plot_type):  # Plots Voltage vs Specific Capacity
    
    if plot_type == 'individual': 
        compare_cycles = individual_cycles()
               
    elif plot_type == 'range':
        a = 0
        while a == 0:
            try:
                print('')
                print('Type the starting and ending cycle of the range you want to graph (separate with a comma)')
                cycle_range = input()
                cycle_start = int(cycle_range.split(",")[0])
                cycle_end = int(cycle_range.split(",")[1])
                for i in range(0, (len(files))):  # Handles input of cycle that hasn't completed
                    if cycle_start < 1 or cycle_end > files[i].num_cycles:
                        print('You entered a cycle that has not been completed yet. Try again.')
                        a = 0
                    else:
                        compare_cycles = range(cycle_start, cycle_end + 1)
                        a = 1
            except ValueError:  # Handles invalid non-integer inputs
                print("That wasn't a correct input for cycle numbers. Try again.")
                a = 0
                print('')
        
    if charge_type == 'charge':
        column_name = 'Charge_Capacity(Ah)'
    elif charge_type == 'discharge':
        column_name = 'Discharge_Capacity(Ah)'
        # column_name is passed as an argument in get_column_info function (below)

    num_of_cycles = len(compare_cycles)
    print("")
    print("...Creating plots.  This may take awhile!...")
    print("")

    # Plot figure set up
    fig = plt.figure()
    ax = plt.subplot(111)
    ax.minorticks_on()
    ax.grid(which='major', linestyle='-', linewidth=.40)
    ax.grid(which='minor', linestyle='-', linewidth=.15)
    plt.title('Voltage Profile')
    plt.ylabel('Voltage (V)')
    plt.xlabel('Specific Capacity (mAh/g)')
    color_list = plot_colors.colors(len(files),len(compare_cycles))
    for i in range(0, (len(files))):
        out_path = (('Outputs//{}_voltage_profile.xlsx').format(files[i].label))
        writer = pd.ExcelWriter(out_path, engine='xlsxwriter')
        sleep(2)
        if charge_type != 'both':
            for cycle in compare_cycles:
                volts = get_column_info(charge_type, 'Voltage(V)', cycle, i)
                capacity = get_column_info(charge_type, column_name, cycle, i)
                spec_cap = ((capacity)/files[i].activeWeight)*1000
                label = (files[i].label + ' Cycle {}'.format(cycle))
                ax.plot(spec_cap, volts, label=label, marker='o', markersize=3, linestyle='None')
                df = pd.DataFrame({'Voltage (V)':volts, 'Capacity (mAh/g)':spec_cap},
                                columns=['Voltage (V)', 'Capacity (mAh/g)'])
                df.to_excel(writer, sheet_name='Cycle {}'.format(cycle))
                
        if charge_type == 'both':
            colors = color_list[i]
            cC = 0

            for cycle in compare_cycles:  # Charge portion data
                charge_volts = get_column_info('charge', 'Voltage(V)', cycle, i)
                charge_cap = get_column_info('charge', 'Charge_Capacity(Ah)', cycle, i)
                charge_spec_cap = ((charge_cap)/files[i].activeWeight)*1000
                df1 = pd.DataFrame({'Charge Voltage (V)':charge_volts, 'Charge Capacity (mAh/g)':charge_spec_cap},
                                columns=['Charge Voltage (V)', 'Charge Capacity (mAh/g)'])
                df1.to_excel(writer, sheet_name='Cycle {}'.format(cycle))
                color = colors[cC]
                label = (files[i].label + ' Cycle {}'.format(cycle))
                ax.plot(charge_spec_cap, charge_volts, marker='o', markersize=3, linestyle='None', label=label, color=color)
                cC = cC + 1
                
            cC = 0  # Resets index of colors back to 0 for discharge plot

            for cycle in compare_cycles:  # Discharge portion data
                discharge_volts = get_column_info('discharge', 'Voltage(V)', cycle, i)
                discharge_cap = get_column_info('discharge', 'Discharge_Capacity(Ah)', cycle, i)
                discharge_spec_cap = ((discharge_cap)/files[i].activeWeight)*1000
                df2 = pd.DataFrame({'Discharge Voltage (V)':discharge_volts,
                                  'Discharge Capacity (mAh/g)':discharge_spec_cap},
                                 columns=['Discharge Voltage (V)', 'Discharge Capacity (mAh/g)'])
                df2.to_excel(writer, sheet_name='Cycle {}'.format(cycle), startcol=5)
                color = colors[cC]
                ax.plot(discharge_spec_cap, discharge_volts, marker='o', markersize=3, linestyle='None', color=color)
                cC = cC + 1        
                
        writer.save()
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * .85, box.height])
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.show()
    print("")
    print("Finished!")
    print("")

def dq_dv(charge_type, h):

    compare_cycles = individual_cycles()
    num_of_cycles = len(compare_cycles)
    print("")
    print("...Creating plots.  This may take awhile!...")
    print("")
    if charge_type == 'charge':
        column_name = 'Charge_Capacity(Ah)'
    elif charge_type == 'discharge':
        column_name = 'Discharge_Capacity(Ah)'
        
    fig = plt.figure()
    ax = plt.subplot(111)
    ax.minorticks_on()
    ax.grid(which='major', linestyle='-', linewidth=.40)
    ax.grid(which='minor', linestyle='-', linewidth=.15)
    plt.title('Differential Capacity')
    plt.ylabel('dQ/dV')
    plt.xlabel('Voltage (V)')
    color_list = plot_colors.colors(len(files),len(compare_cycles))
    for i in range(0, (len(files))):
        out_path = ('Outputs//{}_dQ_dV.xlsx'.format(files[i].label))
        writer = pd.ExcelWriter(out_path, engine='xlsxwriter')
        
        if charge_type != 'both':
            for cycle in compare_cycles:
                diff_cap = []
                diff_volt = []
                volts = get_column_info(charge_type, 'Voltage(V)' ,cycle, i)
                capacity = get_column_info(charge_type, column_name, cycle, i)
                spec_cap = ((capacity)/files[i].activeWeight)*1000

                for num in range(0, len(volts)-h):
                    if ((volts[num+h]) - volts[num]) == 0:
                            pass
                    else:
                        volt_diff = ((volts[num+h]) - volts[num])
                        diff_volt.append(volt_diff)
                        cap_diff = ((spec_cap[num + h]) - spec_cap[num])
                        diff_cap.append(cap_diff)

                dq_dv = np.divide(diff_cap, diff_volt)
                dq_fit = savgol_filter(dq_dv, 21, 2)
                label = (files[i].label + ' Cycle {}'.format(cycle))
                df = pd.DataFrame({'Voltage (V)':volts[0:len(diff_volt)], 'Differential Capacity (dQ/dV)':dq_dv},
                                columns=['Voltage (V)', 'Differential Capacity (dQ/dV)'])
                df.to_excel(writer, sheet_name='Cycle {}'.format(cycle))
                ax.plot(volts[0:len(diff_volt)], dq_fit, '-', label=label)
            writer.save()
            
        if charge_type == 'both':
            cC = 0
            colors = color_list[i]

            for cycle in compare_cycles:  # Charge portion
                diff_cap = []
                diff_volt = []
                volts = get_column_info('charge', 'Voltage(V)', cycle, i)
                capacity = get_column_info('charge', 'Charge_Capacity(Ah)', cycle, i)
                spec_cap = (capacity/files[i].activeWeight)*1000

                for num in range(0, len(volts)-h):
                    if ((volts[num+h]) - volts[num]) == 0:
                            pass
                    else:
                        volt_diff = ((volts[num+h]) - volts[num])
                        cap_diff = ((spec_cap[num + h]) - spec_cap[num])
                        diff_volt.append(volt_diff)
                        diff_cap.append(cap_diff)
                    
                dq_dv = np.divide(diff_cap, diff_volt)
                dq_fit = savgol_filter(dq_dv, 21, 2)
                label = ('{} Cycle {}'.format(files[i].label, cycle))
                color = colors[cC]
                df1 = pd.DataFrame({'Charge Voltage (V)':volts[0:len(diff_volt)],
                                    'Charge Differential Capacity (dQ/dV)':dq_dv},
                                   columns=['Charge Voltage (V)','Charge Differential Capacity (dQ/dV)'])
                df1.to_excel(writer, sheet_name='Cycle {}'.format(cycle))
                ax.plot(volts[0:len(diff_volt)], dq_fit, '-', label=label, color=color)
                cC = cC + 1
            cC = 0  # Resets index of colors back to 0 for discharge plot
            
            for cycle in compare_cycles:  # Discharge portion
                diff_cap = []
                diff_volt = []
                volts = get_column_info('discharge', 'Voltage(V)', cycle, i)
                capacity = get_column_info('discharge', 'Discharge_Capacity(Ah)', cycle, i)
                spec_cap = (capacity/files[i].activeWeight)*1000

                for num in range(0, len(volts)-h):
                    if ((volts[num+h]) - volts[num]) == 0:
                            pass
                    else:
                        volt_diff = ((volts[num+h]) - volts[num])
                        diff_volt.append(volt_diff)
                        cap_diff = ((spec_cap[num + h]) - spec_cap[num])
                        diff_cap.append(cap_diff)
                    
                dq_dv = np.divide(diff_cap, diff_volt)
                dq_fit = savgol_filter(dq_dv, 21, 2)
                color = colors[cC]
                df1 = pd.DataFrame({'Discharge Voltage (V)':volts[0:len(diff_volt)],
                                    'Discharge Differential Capacity (dQ/dV)':dq_dv},
                                   columns=['Discharge Voltage (V)','Discharge Differential Capacity (dQ/dV)'])
                df1.to_excel(writer, sheet_name='Cycle {}'.format(cycle), startcol=5)
                ax.plot(volts[0:len(diff_volt)], dq_fit, '-', color=color)
                cC = cC + 1
                
        writer.save()

    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * .85, box.height])
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.show()
    print("")
    print("Finished!")
    print("")
              
def coulombic_eff(discharge_cap, charge_cap, battery_type):

    # The method of Coulombic eff. calculation depends on battery type
    # The battery type is determined in the class called File
    
    if battery_type == 'half cell':
        col_eff = (charge_cap/discharge_cap) * 100
    elif battery_type == 'full cell':
        col_eff = (discharge_cap/charge_cap) * 100

    return col_eff

def get_capacity(column_name, i):  # Pulls the data from the columns of last worksheet only

    retVal = []
    values = files[i].lastSheet[column_name]
    retVal.append(values)
    retVal = np.array(retVal)
    return retVal

def plot_capacity(charge_type):  # Creates plots and DataFrame/Spreadsheets for specific capacity vs cycle number.
    
    # Set up of the graph figure
    fig = plt.figure()
    ax = plt.subplot(111)
    ax.minorticks_on()
    ax.grid(which='major', axis='y', linestyle='-', linewidth=.40)
    ax.grid(which='minor', axis='y', linestyle='-', linewidth=.15)
    plt.ylabel('Specific Capacity (mAh/g)')
    plt.xlabel('Cycle')
    out_path = ('Outputs//' + str('Capacity_vs_Cycle.xlsx'))
    writer = pd.ExcelWriter(out_path, engine='xlsxwriter')
    
    if File.num_files == 1 and charge_type != 'both':  # If only loading 1 file, plot the cycle efficiency
                            # Does not plot efficiency if more than 1 file loaded (or charge type is both)
        ax2 = ax.twinx()
        ax2.set_ylabel('Coulombic Efficiency (%)', color='red')
        ax2.tick_params(axis='y', labelcolor='red')

    for i in range(0, (len(files))):
        cycle = get_capacity('Cycle_Index', i)
        cycle = cycle.flatten()
        discharge_capacity = get_capacity('Discharge_Capacity(Ah)',i)
        discharge_capacity = discharge_capacity.flatten()
        discharge_spec_capacity = ((discharge_capacity)/(files[i].activeWeight))*1000  # Discharge Specific Capacity
        charge_capacity = get_capacity('Charge_Capacity(Ah)', i)
        charge_capacity = charge_capacity.flatten()
        charge_spec_capacity = ((charge_capacity)/(files[i].activeWeight))*1000  # Charge Specific Capacity
        col_eff = coulombic_eff(discharge_capacity, charge_capacity, files[i].battery_type)

        if charge_type == 'charge':
            plt.title('Charge Specific Capacity vs Cycle')
            label=files[i].label
            ax.plot(cycle, charge_spec_capacity, 'o', label=label)
            if File.num_files == 1:
                ax2.plot(cycle, col_eff, 'r^')
            
        elif charge_type == 'discharge':
            plt.title('Discharge Specific Capacity vs Cycle')
            label=files[i].label
            ax.plot(cycle, discharge_spec_capacity, 'o', label=label)
            if File.num_files == 1:
                ax2.plot(cycle, col_eff, 'r^')
            
        elif charge_type == 'both':
            plt.title('Specific Capacity vs Cycle')
            label_charge = files[i].label + str(' Charge')
            label_discharge = files[i].label + str(' Discharge')
            ax.plot(cycle, charge_spec_capacity, 'o', label=label_charge)
            ax.plot(cycle, discharge_spec_capacity, 'o', label=label_discharge)
            
        df = pd.DataFrame({'Cycle':cycle, 'Charge Capacity (mAh/g)':charge_spec_capacity,
                         'Discharge Capacity (mAh/g)':discharge_spec_capacity, 'Coulombic Efficiency':col_eff},
                        columns=['Cycle','Charge Capacity (mAh/g)', 'Discharge Capacity (mAh/g)','Coulombic Efficiency'])
        df.to_excel(writer, sheet_name=files[i].label)
    writer.save()
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * .85, box.height])
    ax.legend(loc='upper left', bbox_to_anchor=(1.06, .90))
    plt.show()
    print("")
    print("Finished!")
    print("")  

def plot_question():
    
    a = 0
    while a == 0: 
        print('')
        print("CHOOSE THE TYPE OF PLOT YOU WANT TO MAKE:")
        print("(1) Charge voltage profile only")
        print("(2) Discharge voltage profile only")
        print("(3) BOTH charge and discharge voltage profile")
        print("(4) Go back")
        print("")
        print("Type 1, 2, or 3 depending on which kind of profile you want. Type 4 to start over.")
        print("----------")
        answer = input()

        if answer in ['1', '2', '3', '4']:
            question_answer = answer
            a = 1
        else:
            print("That wasn't a valid choice, Try again.")
            a = 0
                
    return question_answer

# USER INTERFACE/CONTROL BELOW:
a = __name__

if a == '__main__':
    initial = time.time()
    files = get_started()
    
    while a == '__main__':
        print('')
        print('YOU CAN CHOOSE FROM THE FOLLOWING:')
        print("")
        print("(1) Plot the voltage profile of a single cycle (or compare multiple individual cycles).")
        print("(2) Plot the voltage profile of a specified range of cycles.")
        print("(3) Plot the differential capacity (dQ/dV)")
        print("(4) Plot the specific capacity vs cycle.")
        print("(5) Quit")
        print("")
        print("Type 1, 2, 3, or 4 depending on which which function you want to perform. Type 5 to exit")
        print("----------")
        result = input()
        if result == '1' or result == '2' or result == '3' or result == '4':
            while result == '1' or result == '2' or result == '3' or result == '4':

                if result == '1':  # Individual voltage profile plot
                    question_answer = plot_question()
                    if question_answer == '1':
                        voltage_profile('charge', 'individual')
                    elif question_answer == '2':
                        voltage_profile('discharge', 'individual')
                    elif question_answer == '3':
                        voltage_profile('both', 'individual')
                    elif question_answer == '4':
                        break
            
                elif result == '2':  # Range voltage profile plot
                    question_answer = plot_question()
                    if question_answer == '1':
                        voltage_profile('charge', 'range')
                    elif question_answer == '2':
                        voltage_profile('discharge', 'range')
                    elif question_answer == '3':
                        voltage_profile('both', 'range')
                    elif question_answer == '4':
                        break
                    
                elif result == '3':  # Diff. capacity plot
                    question_answer = plot_question()
                    if question_answer == '1':
                        dq_dv('charge', 7)
                    elif question_answer == '2':
                        dq_dv('discharge', 7)
                    elif question_answer == '3':
                        dq_dv('both', 7)
                    elif question_answer == '4':
                        break

                if result == '4':  # Capacity vs Cycle plot
                    print('')
                    print('CHOOSE THE TYPE OF PLOT YOU WANT TO MAKE:')
                    print('(1) Charge capacity only.')
                    print('(2) Discharge capacity only.')
                    print('(3) BOTH charge and discharge capacity.')
                    print('(4) Go Back')
                    print('Type 1, 2 or 3 depending on what kind of plot you want. Type 4 to go back.')
                    print('')
                    print("----------")
                    answer = input()
                    if answer == '1':
                        plot_capacity('charge')
                    if answer == '2':
                        plot_capacity('discharge')
                    if answer == '3':
                        plot_capacity('both')
                    if answer == '4':
                        break

                print("Would you like to preform a different plotting function for this file? [y/n]")
                answer = input()
                answer_low = answer.lower()
                if answer_low == 'y' or answer_low == 'yes':
                    result = '0'
                if answer_low == 'n' or answer_low == 'no':
                    final = time.time()
                    print('Completed in {} seconds'.format(final - initial))
                    result = '0'
                    a = 'none'

        elif result == '5':  # Quit
            a = 'none'
            
        else:
            print("")
            print("That wasn't a valid choice, please try again.")
            print("")
            result = 'none'
